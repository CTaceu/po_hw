from pages.base_page import BasePage
from pages.page_locators import MainPageLocators
from pages.page_locators import CartPageLocator


class CartPage(BasePage):
    """CartPage methods"""

    def should_see_sub_menu(self):
        """проверяем что видно sub_menu"""
        assert self.is_element_present(MainPageLocators.MAIN_PAGE_LOCATOR_CHECKER)

    def open_login_page(self):
        """ открытие страницы логина"""
        self.click_on_elem(MainPageLocators.MAIN_PAGE_LOCATOR_LOGIN)

    def open_cart_page(self):
        """ открытие страницы корзины"""
        self.click_on_elem(MainPageLocators.MAIN_PAGE_LOCATOR_CART_BAR)

    def check_items_in_chart(self):
        """ проверка отсутствия элементов в корзине"""
        return self.find_element(CartPageLocator.CART_PAGE_LOCATOR_CART)
