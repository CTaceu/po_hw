import allure

from pages.base_page import BasePage
from pages.page_locators import MainPageLocators
from pages.page_locators import CartPageLocator


class MainPage(BasePage):
    """MainPage methods"""

    def should_see_sub_menu(self):
        """ проверка пристутсвия элемента"""
        with allure.step(f'Ищем элемент {MainPageLocators.MAIN_PAGE_LOCATOR_CHECKER}'):
            assert self.is_element_present(MainPageLocators.MAIN_PAGE_LOCATOR_CHECKER),\
                f'{MainPageLocators.MAIN_PAGE_LOCATOR_CHECKER} отсутствует на странице'

    def open_login_page(self):
        """ открытие страницы логина"""
        with allure.step(f'Клик на  элемент {MainPageLocators.MAIN_PAGE_LOCATOR_LOGIN}'):
            return self.click_on_elem(MainPageLocators.MAIN_PAGE_LOCATOR_LOGIN)

    def open_cart_page(self):
        """ открытие страницы корзины"""
        with allure.step(f'Клик на  элемент {MainPageLocators.MAIN_PAGE_LOCATOR_CART_BAR}'):
            return self.click_on_elem(MainPageLocators.MAIN_PAGE_LOCATOR_CART_BAR)

    def check_items_in_chart(self):
        """ проверка отсутствия элементов в корзине"""
        with allure.step(f'Находим {CartPageLocator.CART_PAGE_LOCATOR_CART} и возвращаем элемент'):
            return self.find_element(CartPageLocator.CART_PAGE_LOCATOR_CART)

