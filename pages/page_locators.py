from selenium.webdriver.common.by import By


class MainPageLocators:
    MAIN_PAGE_LOCATOR_URL = 'http://sp2.iteatester.com'
    MAIN_PAGE_LOCATOR_CHECKER = (By.CSS_SELECTOR, ".sub-menu-wrapper")
    MAIN_PAGE_LOCATOR_LOGIN = (By.XPATH, "/html/body/header/div/div/div[3]/a")
    MAIN_PAGE_LOCATOR_LOGIN_FIELD = (By.CSS_SELECTOR, ".header-login-btn")
    MAIN_PAGE_LOCATOR_CART_BAR = (By.CSS_SELECTOR, ".header-icon")
    MAIN_PAGE_LOCATOR_LOGIN_BAR = (By.CSS_SELECTOR, ".service__name")


class LoginPageLocators:
    LOGIN_PAGE_LOCATOR_URL = 'http://sp2.iteatester.com/login'
    LOGIN_PAGE_LOCATOR = (By.ID, "loginform-username")
    PASSWORD_PAGE_LOCATOR = (By.ID, "loginform-password")
    LOGIN_BTN = (By.CSS_SELECTOR, "#w0 > button:nth-child(3)")


class CartPageLocator:
    CART_PAGE_LOCATOR_URL = 'http://sp2.iteatester.com/cart'
    CART_PAGE_LOCATOR_CART = (By.CSS_SELECTOR, ".header-cart-amount")
    CART_PAGE_BACK_TO_SHOP = (By.CLASS_NAME, "btn-add-to-cart")
