# -*- coding: utf-8 -*-
import time
import allure
import pytest
from pages.page_locators import LoginPageLocators
from pages.login_page import LoginPage


class Test:
    """"""
    main_page_url = LoginPageLocators.LOGIN_PAGE_LOCATOR_URL

    @allure.story("Заходим на страницу логина и проверяем что есть кнопка логина")
    def test_go_to_login_page(self, browser):
        with allure.step(f'Загружаем страницу логина'):
            login_page = LoginPage(browser, self.main_page_url)
            login_page.get_page()
            with allure.step(f'Проверяем что есть кнопка логина'):
                time.sleep(2)
                assert login_page.should_be_login_form() == True, \
                    f'should_be_login_form returned False -> element_not_present'

    @allure.story("Заходим на страницу логина и логинимся")
    def test_user_login_can_login(self, browser):
        login_page = LoginPage(browser, self.main_page_url)
        login_page.get_page()
        with allure.step(f'Выполняем метод login'):
            login_page.login()
            with allure.step(f'Проверяем что залогинились сверяя Title == My Account '):
                time.sleep(2)
                assert login_page.get_title() == 'My Account'


if __name__ == "__main__":
    pytest.main()
