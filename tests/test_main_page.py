import time
import allure
import pytest
from pages.page_locators import MainPageLocators
from pages.main_page import MainPage


class Test:
    """"""
    main_page_url = MainPageLocators.MAIN_PAGE_LOCATOR_URL

    @allure.story("Проверяем что элемент главной страницы доступен")
    def test_should_see_sub_menu(self, browser):
        with allure.step(f'Переходим на : {self.main_page_url}'):
            main_page = MainPage(browser, self.main_page_url)
            main_page.get_page()
            time.sleep(2)
            with allure.step(f'Проверяем что элемент главной страницы доступен и мы точно на ней.'):
                main_page.should_see_sub_menu()

    @allure.story("Проверяем Title главной страницы")
    def test_go_to_main_page(self, browser):
        with allure.step(f'Переходим на : {self.main_page_url}'):
            main_page = MainPage(browser, self.main_page_url)
            main_page.get_page()
            with allure.step(f'Проверяем Title главной страницы == Online Store | ITEA '):
                assert main_page.get_title() == 'Online Store | ITEA',\
                    f'{main_page.get_title()} != Online Store | ITEA'

    @allure.story("Проверяем открытие страницы логина")
    def test_user_can_open_login_page(self, browser):
        with allure.step(f'Переходим на : {self.main_page_url}'):
            main_page = MainPage(browser, self.main_page_url)
            main_page.get_page()
            with allure.step(f'Открываем страницу логина и проверяем что Title == Login'):
                main_page.open_login_page()
                assert main_page.get_title() == 'Login', f'{main_page.get_title()} != Login'

    @allure.story("Проверяем что юзер может открыть корзину")
    def test_user_can_open_cart_page(self, browser):
        with allure.step(f'Переходим на : {self.main_page_url}'):
            main_page = MainPage(browser, self.main_page_url)
            main_page.get_page()
            with allure.step(f'Открываем страницу корзины и проверяем что Title == Shopping Cart'):
                main_page.click_on_elem(MainPageLocators.MAIN_PAGE_LOCATOR_CART_BAR)
                assert main_page.get_title() == 'Shopping Cart', f'{main_page.get_title()} != Shopping Cart'

    @allure.story("Проверяем что товаров в корзине 0")
    def test_cart_empty(self, browser):
        with allure.step(f'Переходим на : {self.main_page_url}'):
            main_page = MainPage(browser, self.main_page_url)
            main_page.get_page()
            with allure.step(f'Открываем козину'):
                main_page.open_cart_page()
                with allure.step(f'Проверяем что значение элемента = 0'):
                    assert main_page.check_items_in_chart().text == '0', \
                        f'В корзине {main_page.check_items_in_chart().text} предметов'


if __name__ == "__main__":
    pytest.main()
