# -*- coding: utf-8 -*-
import time
import allure
import pytest
from pages.cart_page import CartPage
from pages.page_locators import CartPageLocator


class Test:
    """"""
    main_page_url = CartPageLocator.CART_PAGE_LOCATOR_URL

    @allure.story("Заходим на страницу корзины и сравниваем Title")
    def test_go_to_cart_page(self, browser):
        with allure.step("Переходим в корзину и сравниваем Title"):
            cart_page = CartPage(browser, self.main_page_url)
            cart_page.get_page()
            with allure.step(f'Проверяем что перешли в корзину сверяя Title == Shopping Cart '):
                time.sleep(1)
                assert cart_page.get_title() == 'Shopping Cart',\
                    f'{cart_page.get_title()} != Shopping Cart'

    @allure.story("Возвращаемся со страницы корзины в магазин по кнопке Go shop")
    def test_back_to_shop_page(self, browser):
        with allure.step(f'Переходим на : {self.main_page_url}'):
            cart_page = CartPage(browser, self.main_page_url)
            cart_page.get_page()
            with allure.step(f"Нажимаем кнопку вернуться в магазин {CartPageLocator.CART_PAGE_BACK_TO_SHOP}"):
                cart_page.click_on_elem(CartPageLocator.CART_PAGE_BACK_TO_SHOP)
                with allure.step(f'Проверяем что вернулись в магазин сверяя Title == Products '):
                    assert cart_page.get_title() == 'Products', \
                        f'{cart_page.get_title()} != Products'


if __name__ == "__main__":
    pytest.main()
