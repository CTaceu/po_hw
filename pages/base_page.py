import allure
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BasePage:
    """BasePage methods"""

    def __init__(self, driver, url):
        self.driver = driver
        self.base_url = url

    def get_page(self):
        """открытие страницы"""
        self.driver.get(self.base_url)

    def go_to_site(self):
        """ открытие страницы"""
        with allure.step(f'Открываем {self.driver.get(self.base_url)}'):
            return self.driver.get(self.base_url)

    def get_title(self):
        """получение заголовка страницы"""
        with allure.step(f'Возвращаем Title страницы {self.driver.title}'):
            return self.driver.title

    def find_element(self, locator, time=5):
        """поиск элемента на странице"""
        with allure.step(f'Ищем элемент  {locator}'):
            return WebDriverWait(self.driver, time).until(EC.element_to_be_clickable(locator),
                                                          message=f"Can't find element by locator {locator}")

    def find_elements(self, locator, time=5):
        """поиск элементов на странице"""
        with allure.step(f'Ищем элементы  {locator}.'):
            return WebDriverWait(self.driver, time).until(EC.presence_of_all_elements_located(locator),
                                                          message=f"Can't find elements by locator {locator}")

    def send_keys(self, locator, key=None, ):
        """отправка текста в поля ввода"""
        with allure.step(f'Ищем элемент  {locator} и используя send_keys отправляем {key}.'):
            element = self.find_element(locator)
            if element:
                element.send_keys(key)

            else:
                msg = 'Element with locator {0} not found'
                raise AttributeError(msg.format(locator))

    def click_on_elem(self, locator):
        """нажатие на элемент"""
        with allure.step(f'Ищем элемент  {locator} и кликаем его.'):
            return self.find_element(locator).click(), f"Can't find element by locator {locator}"

    def is_element_present(self, locator, timeout=4):
        """проверка присутствия элемента"""
        with allure.step(f'Ищем элемент  {locator}'):
            try:
                WebDriverWait(self.driver, timeout).until(EC.presence_of_element_located(locator))
            except NoSuchElementException:
                return False
            return True

    def is_not_element_present(self, locator, timeout=4):
        """проверка отсутсвия элемента"""
        with allure.step(f'Ищем элемент  {locator}'):
            try:
                WebDriverWait(self.driver, timeout).until(EC.presence_of_element_located(locator))
            except TimeoutException:
                return True
            return False
