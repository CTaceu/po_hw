# -*- coding: utf-8 -*-
"""should_be_login_url
should_be_login_form
should_be_register_form"""
from pages.base_page import BasePage
from pages.page_locators import LoginPageLocators


class LoginPage(BasePage):
    """LoginPage methods"""

    def should_be_login_form(self):
        """проверка, что есть кнопка логина"""
        return self.is_element_present(LoginPageLocators.LOGIN_BTN)

    def login(self):
        """логин на сайте """
        self.send_keys(LoginPageLocators.LOGIN_PAGE_LOCATOR, key='ct')
        self.send_keys(LoginPageLocators.PASSWORD_PAGE_LOCATOR, key='12345678Aa')
        self.find_element(LoginPageLocators.LOGIN_BTN).click()



