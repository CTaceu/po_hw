import logging
import logging.config
from selenium import webdriver
import pytest
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager

s = Service(executable_path=ChromeDriverManager().install())
chrome_options = Options()

chrome_options.add_argument("--headless")
chrome_options.add_argument('window-size=1920x1080')
chrome_options.add_argument('--disable-gpu')
chrome_options.add_argument('--enable-javascript')
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--ignore-certificate-errors')


@pytest.fixture(autouse=True, scope='class')
def browser():
    web_driver = webdriver.Chrome(ChromeDriverManager().install(), options=chrome_options)
    web_driver.maximize_window()
    web_driver.implicitly_wait(4)

    yield web_driver

    web_driver.quit()
    print('\nquit browser!')


@pytest.fixture(scope='class')
def log1_get():
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)
    fh = logging.FileHandler(filename='../tests/results/Result.log', mode='w')
    fh.setLevel(logging.INFO)
    formatter = logging.Formatter('%(message)s')
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    return logger

